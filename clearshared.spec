Name:      clearshared
Version:   0.0.12
Release:   1%{?dist}
License:   Proprietary
Summary:   Disk space rental management using ClearSHARE
BuildRoot: %{_builddir}/%{name}
BuildArch: x86_64

Url:     https://gitlab.com/clearos/clearfoundation/clearshared
Group:   Development/Libraries

Source0: clearshare.tar.gz
Source1: clearshared.service

Requires: rh-python38

%description
ClearSHARE Decentralized Storage

%define _VENV_FOLDER clearshare
%define _VENV /var/clearos/envs/%{_VENV_FOLDER}/lib/python3.8/site-packages/%{_VENV_FOLDER}
%define _GIT_COMMIT 1d1f48941c6e4a73f9b254e517ab82218655e0ad

%prep
[ -d %{buildroot} ] && rm -rf %{buildroot}
  
%setup -n %{_VENV_FOLDER}-%{_GIT_COMMIT}

%install

mkdir -p -m 755 %{buildroot}/%{_VENV}/%{_VENV_FOLDER}

cp -r %{_VENV_FOLDER}/* %{buildroot}/%{_VENV}/%{_VENV_FOLDER}

install -m 0644 logging.conf %{buildroot}%{_VENV}/logging.conf
install -m 0644 requirements.txt %{buildroot}%{_VENV}/requirements.txt
install setup.py %{buildroot}%{_VENV}/setup.py
install -D -m 0644 %{SOURCE1} %{buildroot}%{_unitdir}/%{name}.service


# Exit install without trying to compile the python code
exit 0

%pre
[ -d "/var/clearos/envs/%{_VENV_FOLDER}" ] && rm -rf /var/clearos/envs/%{_VENV_FOLDER}
exit 0

%preun
# Uninstall only
%systemd_preun %{name}

%post
cd /var/clearos/envs/
/usr/bin/scl enable rh-python38 'python3 -m venv %{_VENV_FOLDER}'
source %{_VENV_FOLDER}/bin/activate
pip install -q -e %{_VENV} &>/dev/null
deactivate

# Install only:
%systemd_post %{name}

# Upgrade only
if [ $1 -eq 2 ] ; then 
        # Package upgrade, not uninstall
        systemctl daemon-reload
        systemctl is-enabled %{name} -q && systemctl restart %{name} -q >/dev/null 2>&1
fi

exit 0

%postun
if [ $1 -eq 0 ]; then
    [ -d "/var/clearos/envs/%{_VENV_FOLDER}" ] && rm -rf /var/clearos/envs/%{_VENV_FOLDER}
fi
%systemd_postun
# Upgrade only:
#%systemd_postun_with_restart %{name}
exit 0

%files
%exclude /README.md
%defattr(-,root,root,-)
%{_VENV}/
%{_unitdir}/%{name}.service

%changelog
* Mon Mar 28 2022 Nick Howitt <nhowitt@clearcenter.com> 0.0.12
- Update permissions

* Mon Mar 28 2022 Nick Howitt <nhowitt@clearcenter.com> 0.0.11
- Update permissions

* Mon Mar 21 2022 Nick Howitt <nhowitt@clearcenter.com> 0.0.10
- Update clearshare tarball

* Thu Jan 20 2022 Nick Howitt <nhowitt@clearcenter.com> 0.0.9
- Update clearshare tarball

* Thu Jan 13 2022 Nick Howitt <nhowitt@clearcenter.com> 0.0.8
- Update clearshare tarball

* Wed Jan 12 2022 Nick Howitt <nhowitt@clearcenter.com> 0.0.7
- Update clearshare tarball

* Mon Jan 10 2022 Nick Howitt <nhowitt@clearcenter.com> 0.0.6
- Update clearshare tarball

* Mon Jan 03 2022 Nick Howitt <nhowitt@clearcenter.com> 0.0.5
- Update clearshare tarball

* Tue Oct 19 2021 Nick Howitt <nhowitt@clearcenter.com> 0.0.1
- Upstream version 0.0.1 initial commit

# Params : vim -> :set ts=4 sw=4
