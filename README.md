Upgrading the underlying clearlifed package
==
To upgrade the underlying clearshare package:
- Go to https://gitlab.com/clearos/clearfoundation/clearshare > History.
- Go in to browse the files at the most recent commit and download it as a tarball.
- Copy the tarball (which has the commit hash in the file name) into the deploy/ folder, removing the old file and the commit hash from the new file name.
- Update the file name and sha256sum in sources.download.
- Update the _GIT_COMMIT variable with the new commit hash of clearlife in the spec file.
- Bump the version in the spec file.
